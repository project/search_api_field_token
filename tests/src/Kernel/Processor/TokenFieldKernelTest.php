<?php

namespace Drupal\Tests\search_api_field_token\Kernel\Processor;

use Drupal\node\Entity\Node;
use Drupal\search_api\Item\Field;
use Drupal\search_api\Utility\Utility;
use Drupal\Tests\search_api\Kernel\Processor\ProcessorTestBase;

/**
 * Tests the "Token field" processor at a higher level.
 *
 * @group search_api_field_token
 *
 * @coversDefaultClass \Drupal\search_api_field_token\Plugin\search_api\processor\TokenField
 */
class TokenFieldKernelTest extends ProcessorTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'search_api_field_token',
  ];

  /**
   * The nodes created for testing.
   *
   * @var \Drupal\node\Entity\Node[]
   */
  protected $nodes;

  /**
   * {@inheritdoc}
   */
  public function setUp($processor = NULL): void {
    parent::setUp('token_field');

    $token_field = new Field($this->index, 'token_field');
    $token_field->setType('string');
    $token_field->setPropertyPath('token_field');
    $token_field->setLabel('Token: Item Type');
    $token_field->setConfiguration(['token' => '[node:type]']);
    $this->index->addField($token_field);
    $this->index->save();

    $this->nodes[0] = Node::create([
      'title' => 'Test',
      'type' => 'article',
    ]);
    $this->nodes[0]->save();
  }

  /**
   * Tests extracting the field for a search item.
   *
   * @covers ::addFieldValues
   */
  public function testItemFieldExtraction() {
    $node = $this->nodes[0];
    $id = Utility::createCombinedId('entity:node', $node->id() . ':en');
    $item = \Drupal::getContainer()
      ->get('search_api.fields_helper')
      ->createItemFromObject($this->index, $node->getTypedData(), $id);

    // Extract field values and check the value of the Token field.
    $fields = $item->getFields();
    $expected = [$node->bundle()];
    $this->assertEquals($expected, $fields['token_field']->getValues());
  }

}
