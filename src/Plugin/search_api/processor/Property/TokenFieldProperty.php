<?php

namespace Drupal\search_api_field_token\Plugin\search_api\processor\Property;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Processor\ConfigurablePropertyBase;

/**
 * Defines a "token field" property.
 *
 * @see \Drupal\search_api_field_token\Plugin\search_api\processor\TokenField
 */
class TokenFieldProperty extends ConfigurablePropertyBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'token' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(FieldInterface $field, array $form, FormStateInterface $form_state) {
    $configuration = $field->getConfiguration();

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Provide token field value'),
      '#description' => $this->t('Use this field to set the data to be sent to the index using tokens.'),
      '#default_value' => $configuration['token'] ?? FALSE,
      '#return_value' => TRUE,
    ];
    return $form;
  }

}
