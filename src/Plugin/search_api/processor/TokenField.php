<?php

namespace Drupal\search_api_field_token\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityInterface;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api_field_token\Plugin\search_api\processor\Property\TokenFieldProperty;

/**
 * Adds the item's field token to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "token_field",
 *   label = @Translation("Token field"),
 *   description = @Translation("Adds the item's field token to the indexed data."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class TokenField extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Token'),
        'description' => $this->t('A field token where the item can be accessed'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['token_field'] = new TokenFieldProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    // Get all of the token fields on our item.
    $token_fields = $this->getFieldsHelper()
      ->filterForPropertyPath($item->getFields(), NULL, 'token_field');
    // Get the entity object, bail if there's somehow not one.
    $entity = $item->getOriginalObject()->getValue();
    if (!$entity || !$entity instanceof EntityInterface) {
      // Apparently we were active for a wrong item.
      return;
    }

    foreach ($token_fields as $token_field) {
      $config = $token_field->getConfiguration();
      if (!empty($config['token'])) {
        $token = \Drupal::token();
        // Make sure the token value has been replaced.
        if ($field_value = $token->replace($config['token'], [$entity->getEntityTypeId() => $entity])) {
          // Make sure the token value has been replaced.
          if ($field_value != $config['token']) {
            $token_field->addValue($field_value);
          }
        }
      }
    }
  }

}
