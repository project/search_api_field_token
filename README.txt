INTRODUCTION
------------
The module provides a token field option on Search API index to set the data to be sent to the index using tokens.


REQUIREMENTS
------------
This module requires the following modules:

 * [Search API](https://www.drupal.org/project/search_api)
 * [Token](https://www.drupal.org/project/token)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
------------

* Configure the Search API field tokens in Administration » Configuration » Search and Metadata » Search API:

   - Configure the index and add a new field of field token into the index

MAINTAINERS
-----------

Current maintainers:
 * Naveen Valecha (naveenvalecha) - https://www.drupal.org/u/naveenvalecha

This project has been sponsored by:
 * Morpht
   Visit https://www.morpht.com for more information.
